<?php

namespace DreamCat\FrameCore\Protocol;

use DreamCat\FrameCore\ServerMessage\Response\JsonExResponse;
use DreamCat\FrameInterface\Controller\ProtocolInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * json接口协议
 * @author vijay
 */
class JsonApiProtocol implements ProtocolInterface
{
    /**
     * 输入转换
     * @param ServerRequestInterface $request 请求对象
     * @return ServerRequestInterface 转换后的请求对象
     */
    public function convertInput(ServerRequestInterface $request): ServerRequestInterface
    {
        return $request;
    }

    /**
     * 输出转换
     * @param ResponseInterface $response 响应消息
     * @return ResponseInterface 响应消息
     */
    public function convertOutput(ResponseInterface $response): ResponseInterface
    {
        return $response;
    }

    /**
     * 格式化控制器方法的输出值
     * @param mixed $response 控制器方法返回值
     * @return ResponseInterface 标准响应消息
     */
    public function formatOutput($response): ResponseInterface
    {
        return new JsonExResponse($response);
    }
}

# end of file
