<?php

namespace DreamCat\FrameCore\Annotation\OutputFormater;

use DreamCat\AnnotationParser\IAnnotationParse;
use DreamCat\AnnotationParser\IDocParseResultFormater;

/**
 * 控制器方法注解解析输出格式化
 * @author vijay
 */
class ControllerMethodParseOutput implements IDocParseResultFormater
{
    /** @var array 输出结果 */
    private $result = [];

    /**
     * 重置
     * @return void
     */
    public function reset(): void
    {
        $this->result = [];
    }

    /**
     * 添加输出结果
     * @param IAnnotationParse $annotationParse 解析器
     * @param string $key 注解函数
     * @param mixed $result 解析结果
     * @return void
     */
    public function addResult(IAnnotationParse $annotationParse, string $key, $result): void
    {
        if (!is_array($result) || !isset($result["var"])) {
            return;
        }
        $this->result[$result["var"]] = $result;
    }

    /**
     * 获取输出结果
     * @return array 输出结果，结构由实现类决定
     */
    public function getResult(): array
    {
        return $this->result;
    }
}

# end of file
