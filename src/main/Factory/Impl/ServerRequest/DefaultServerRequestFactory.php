<?php

namespace DreamCat\FrameCore\Factory\Impl\ServerRequest;

use DreamCat\Container\EntryFactory;
use Zend\Diactoros\ServerRequestFactory;

/**
 * 默认的服务器请求对象生成器
 * @author vijay
 */
class DefaultServerRequestFactory implements EntryFactory
{
    /**
     * 创建实体
     * @param string $id 实体标识
     * @param string $arg 额外参数
     * @return mixed 实体
     */
    public function create(string $id, string $arg)
    {
        return ServerRequestFactory::fromGlobals();
    }
}

# end of file
