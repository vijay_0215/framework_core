<?php

namespace DreamCat\FrameCore\Factory\Impl\AnnotationDealer;

use DreamCat\Container\AnnotationDealer;
use DreamCat\Container\Container;

/**
 * RootPath注解处理器
 * @author vijay
 */
class RootDirPath implements AnnotationDealer
{
    /** @var string 项目根目录 */
    private $rootPath;

    /**
     * RootDirPath constructor.
     * @param string $rootPath 项目根目录
     */
    public function __construct(string $rootPath)
    {
        $this->rootPath = $rootPath;
    }

    /**
     * get
     * 获取注解生成的值
     * @param Container $container 容器
     * @param array $args 注解参数
     * @return string 生成的值
     */
    public function get(Container $container, array $args)
    {
        return $this->rootPath;
    }
}

# end of file
