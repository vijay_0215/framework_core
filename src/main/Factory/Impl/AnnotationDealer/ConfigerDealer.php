<?php

namespace DreamCat\FrameCore\Factory\Impl\AnnotationDealer;

use DreamCat\Container\AnnotationDealer;
use DreamCat\Container\Container;
use DreamCat\FrameInterface\ConfigReader;

/**
 * Config注解处理器，从配置中读取数据，用于注入
 * @author vijay
 */
class ConfigerDealer implements AnnotationDealer
{
    /** @var ConfigReader 配置读取器 */
    private $configReader;

    /**
     * ConfigerDealer constructor.
     * @param ConfigReader $configReader 配置读取器
     */
    public function __construct(ConfigReader $configReader)
    {
        $this->configReader = $configReader;
    }

    /**
     * get
     * 获取注解生成的值
     * @param Container $container 容器
     * @param array $args 注解参数
     * @return mixed 生成的值
     */
    public function get(Container $container, array $args)
    {
        $key = array_shift($args);
        $params = $args ? implode(" ", $args) : null;
        return $this->configReader->get($key, $params);
    }
}

# end of file
