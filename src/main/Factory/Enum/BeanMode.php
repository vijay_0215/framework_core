<?php

namespace DreamCat\FrameCore\Factory\Enum;

use MyCLabs\Enum\Enum;

/**
 * bean 的构建模式
 * @author vijay
 * @method static BeanMode BEAN_MODE_FACTORY()
 * @method static BeanMode BEAN_MODE_CLASS()
 */
class BeanMode extends Enum
{
    /** @var string 装配模式之工厂模式 */
    const BEAN_MODE_FACTORY = "factory";
    /** @var string 装配模式之对象模式 */
    const BEAN_MODE_CLASS = "class";
}

# end of file
