<?php

namespace DreamCat\FrameCore\Helper\ConfigHelper;

/**
 * 帮助快速写路由配置的方法
 * @author vijay
 */
class RouterConfig
{
    /**
     * 生成POST路由配置
     * @param string $ctl 控制器类名
     * @param string $act 方法名
     * @return array 配置内容
     */
    public static function post(string $ctl, string $act = "index"): array
    {
        return self::method("POST", $ctl, $act);
    }

    /**
     * 生成路由配置
     * @param string $method http方法
     * @param string $ctl 控制器类名
     * @param string $act 方法名
     * @return array 配置内容
     */
    public static function method(string $method, string $ctl, string $act = "index"): array
    {
        $handle = $ctl;
        if ($act != "" && $act != "index") {
            $handle .= "::{$act}";
        }
        return [
            "method" => $method,
            "handle" => $handle,
        ];
    }

    /**
     * 生成GET路由配置
     * @param string $ctl 控制器类名
     * @param string $act 方法名
     * @return array 配置内容
     */
    public static function get(string $ctl, string $act = "index"): array
    {
        return self::method("GET", $ctl, $act);
    }
}

# end of file
