<?php

namespace DreamCat\FrameCore\Helper\ConfigHelper;

use DreamCat\FrameCore\Factory\Enum\BeanMode;

/**
 * bean配置辅助
 * @author vijay
 */
class BeansConfig
{
    /**
     * 生成工厂模式的配置
     * @param string $class 类名
     * @param array $props 注入的属性列表
     * @param array $alias 别名列表
     * @param bool $save 是否保存在容器中
     * @return array 配置内容
     */
    public static function factory(string $class, array $props = [], array $alias = [], bool $save = true): array
    {
        return self::value(BeanMode::BEAN_MODE_FACTORY, $class, $props, $alias, $save);
    }

    /**
     * 生成配置
     * @param string $mode 模式
     * @param string $class 类名
     * @param array $props 注入的属性列表
     * @param array $alias 别名列表
     * @param bool $save 是否保存在容器中
     * @return array 配置内容
     */
    private static function value(string $mode, string $class, array $props, array $alias, bool $save = true): array
    {
        $ret = [
            "mode" => $mode,
            "class" => $class,
        ];
        if ($props) {
            $ret["props"] = $props;
        }
        if ($alias) {
            $ret["alias"] = $alias;
        }
        if (!$save) {
            $ret["save"] = $save;
        }
        return $ret;
    }

    /**
     * 生成类模式的配置
     * @param string $class 类名
     * @param array $props 注入的属性列表
     * @param array $alias 别名列表
     * @param bool $save 是否保存在容器中
     * @return array 配置内容
     */
    public static function byClass(string $class, array $props = [], array $alias = [], bool $save = true): array
    {
        return self::value(BeanMode::BEAN_MODE_CLASS, $class, $props, $alias, $save);
    }
}

# end of file
