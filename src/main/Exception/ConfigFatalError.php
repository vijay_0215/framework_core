<?php

namespace DreamCat\FrameCore\Exception;

/**
 * 配置文件引发的致命错误
 * @author vijay
 */
class ConfigFatalError extends \RuntimeException
{
}

# end of file
