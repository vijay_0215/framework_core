<?php

namespace DreamCat\FrameCore\ServerMessage\Response;

use Dreamcat\Class2Array\Impl\DefaultJsonValueFixer;
use Dreamcat\Class2Array\JsonValueFixer;
use Zend\Diactoros\Response\JsonResponse;

/**
 * 扩展的json响应，可以编码对象
 * @author vijay
 */
class JsonExResponse extends JsonResponse
{
    /** @var JsonValueFixer $jsonEncoder */
    private $jsonValueFixer;

    /**
     * JsonExResponse constructor.
     * @param $data
     * @param int $status
     * @param array $headers
     * @param int $encodingOptions
     * @param JsonValueFixer|null $jsonValueFixer
     */
    public function __construct(
        $data,
        int $status = 200,
        array $headers = [],
        int $encodingOptions = self::DEFAULT_JSON_FLAGS,
        JsonValueFixer $jsonValueFixer = null
    ) {
        if (!$jsonValueFixer) {
            $jsonValueFixer = new DefaultJsonValueFixer();
        }
        $this->setJsonValueFixer($jsonValueFixer);
        parent::__construct($this->getJsonValueFixer()->fixValue($data), $status, $headers, $encodingOptions);
    }

    /**
     * @return JsonValueFixer
     */
    public function getJsonValueFixer(): JsonValueFixer
    {
        return $this->jsonValueFixer;
    }

    /**
     * @param JsonValueFixer $jsonValueFixer
     * @return static 对象本身
     */
    public function setJsonValueFixer(JsonValueFixer $jsonValueFixer): JsonExResponse
    {
        $this->jsonValueFixer = $jsonValueFixer;
        return $this;
    }
}

# end of file
