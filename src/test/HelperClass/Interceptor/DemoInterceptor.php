<?php

namespace DreamCat\FrameCore\HelperClass\Interceptor;

use DreamCat\FrameInterface\HttpHandle\InterceptorInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\JsonResponse;

/**
 * 测试拦截器用的类
 * @author vijay
 */
class DemoInterceptor implements InterceptorInterface
{
    /**
     * 拦截，如果返回null则继续往下走，否则返回响应
     * @param string $controllerClass 控制器类名
     * @param string $actName 方法名
     * @param ServerRequestInterface $request 服务器请求
     * @return ResponseInterface 响应消息
     */
    public function httpInterceptor(
        string $controllerClass,
        string $actName,
        ServerRequestInterface $request
    ): ?ResponseInterface {
        return new JsonResponse(
            [
                "body" => $request->getParsedBody(),
                "ctl" => $controllerClass,
                "act" => $actName,
            ],
            599
        );
    }
}

# end of file
