<?php

namespace DreamCat\FrameCore\HelperClass\Protocol;

use DreamCat\FrameCore\HelperClass\Vo\OutputVo;
use DreamCat\FrameCore\Protocol\JsonApiProtocol;
use Psr\Http\Message\ResponseInterface;

/**
 * 测试用的协议
 * @author vijay
 */
class DemoProtocol extends JsonApiProtocol
{
    /**
     * @param mixed $response 控制器输出
     * @return ResponseInterface 修改后的输出
     */
    public function formatOutput($response): ResponseInterface
    {
        if ($response instanceof OutputVo) {
            $response->setKey($response->getKey() . "::suffer");
        }
        return parent::formatOutput($response);
    }
}

# end of file
