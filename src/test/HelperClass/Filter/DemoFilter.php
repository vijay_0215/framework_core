<?php

namespace DreamCat\FrameCore\HelperClass\Filter;

use DreamCat\FrameInterface\HttpHandle\FilterInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\TextResponse;

/**
 * 测试用的过滤器
 * @author vijay
 */
class DemoFilter implements FilterInterface
{
    /**
     * 检查过滤请求，如果返回null则继续往下走，否则返回响应
     * @param ServerRequestInterface $request 服务器请求
     * @return ResponseInterface 响应消息
     */
    public function httpFilter(ServerRequestInterface $request): ?ResponseInterface
    {
        return new TextResponse(serialize($request->getParsedBody()), 305);
    }
}

# end of file
