<?php

namespace DreamCat\FrameCore\HelperClass\Controller;

use DreamCat\FrameCore\HelperClass\Bean\DemoBeanAlias;
use Zend\Diactoros\Response\TextResponse;

/**
 * 流程测试控制器
 * @author vijay
 */
class FlowController
{
    /**
     * @RootPath
     * @var string 项目根目录
     */
    private $rootPath;
    /**
     * @Config db/name mysql
     * @var string
     */
    private $dbName;
    /**
     * @Config db/user root
     * @var string
     */
    private $dbUser;

    /**
     * @return string
     */
    public function index()
    {
        return "{$this->rootPath}::{$this->dbName}/{$this->dbUser}";
    }

    /**
     * @Autowire alias
     * @var DemoBeanAlias
     */
    private $bean;

    /**
     * @param string $raw @PathVariable(raw)
     * @return TextResponse -
     */
    public function raw(string $raw)
    {
        return new TextResponse($raw . "::" . $this->bean->id, 302);
    }
}

# end of file
