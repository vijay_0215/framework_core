<?php

namespace DreamCat\FrameCore\HelperClass\Controller;

use DreamCat\FrameCore\HelperClass\Vo\ComplexVO;
use DreamCat\FrameCore\HelperClass\Vo\DemoVo;
use DreamCat\FrameCore\HelperClass\Vo\OutputVo;
use stdClass;

/**
 * 示例控制器
 * @author vijay
 */
class DemoController
{
    /**
     * @param string $label @PathVariable(label)
     * @param int $page
     * @param array $body @RequestBody
     * @param array $path @PathVariable
     * @param int $pageSize
     * @return void
     */
    public function demo(string $label, int $page, array $body, array $path, int $pageSize = 10)
    {
    }

    /**
     * -
     * @param stdClass $body @RequestBody
     * @param DemoVo $get @GetParam
     * @param DemoVo $path @PathVariable
     * @return void
     */
    public function testStd(stdClass $body, DemoVo $get, DemoVo $path)
    {
    }

    /**
     * @param mixed $p1 @GetParam(p)
     * @param $p2
     * @param string $body @RequestBody
     * @param string $autoPath
     * @param mixed $allGet @GetParam
     * @return void
     */
    public function index($p1, $p2, string $body, string $autoPath, $allGet)
    {
    }

    /**
     * @param string $field @RequestBody(field)
     * @param mixed $autoV1
     * @param int $autoV2
     * @param string $autoV3
     * @return void
     */
    public function testPost($field, $autoV1, int $autoV2, string $autoV3)
    {
    }

    /**
     * @param string $str @RequestBody(s)
     * @param int $int @RequestBody(i)
     * @param float $float @RequestBody(f)
     * @param bool $bool @RequestBody(b)
     * @param array $ary @RequestBody(a)
     * @return void
     */
    public function testBodyField(string $str, int $int, float $float, bool $bool, array $ary)
    {
    }

    /**
     * @param callable $call @RequestBody(c)
     * @return void
     */
    public function testBodyCall(callable $call)
    {
    }

    /**
     * @param stdClass $var @RequestBody(c)
     * @return void
     */
    public function testNotStdClass(stdClass $var)
    {
    }

    /**
     * @param DemoVo $vo @RequestBody
     * @return void
     */
    public function testDemoVo(DemoVo $vo)
    {
    }

    /**
     * @param ComplexVO $vo
     * @return void
     */
    public function testComplexVo(ComplexVO $vo)
    {
    }

    /**
     * @param string $key
     * @return OutputVo
     */
    public function protocol(string $key = "default"): OutputVo
    {
        return new OutputVo($key);
    }
}

# end of file
