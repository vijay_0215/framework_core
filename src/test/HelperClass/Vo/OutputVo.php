<?php

namespace DreamCat\FrameCore\HelperClass\Vo;

/**
 * 测试用的输出vo
 * @author vijay
 */
class OutputVo
{
    /** @var mixed */
    private $key;

    /**
     * OutputVo constructor.
     * @param mixed $key 赋值给key
     */
    public function __construct($key = null)
    {
        $this->key = $key;
    }

    /**
     * @return mixed
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param mixed $key
     * @return OutputVo
     */
    public function setKey($key)
    {
        $this->key = $key;
        return $this;
    }
}

# end of file
