<?php

namespace DreamCat\FrameCore\HelperClass\Vo;

/**
 * 示例vo
 * @author vijay
 */
class DemoVo
{
    /** @var int - */
    private $page;
    /** @var int - */
    private $size;

    /**
     * DemoVo constructor.
     * @param int $page -
     * @param int $size -
     */
    public function __construct(int $page = 1, int $size = 100)
    {
        $this->page = $page;
        $this->size = $size;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @return int
     */
    public function getSize(): int
    {
        return $this->size;
    }
}

# end of file
