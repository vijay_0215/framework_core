<?php

namespace DreamCat\FrameCore\HelperClass;

use Psr\Http\Message\ResponseInterface;
use Zend\HttpHandlerRunner\Emitter\EmitterInterface;

/**
 * 测试 emit
 * @author -
 */
class TestEmit implements EmitterInterface
{
    /** @var ResponseInterface */
    public static $lastResponse;
    /**
     * @param ResponseInterface $response
     * @return bool
     */
    public function emit(ResponseInterface $response): bool
    {
        self::$lastResponse = $response;
        return true;
    }
}

# end of file
