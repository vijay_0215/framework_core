<?php

namespace DreamCat\FrameCore\Cases\Helper\ConfigHelper;

use DreamCat\FrameCore\Helper\ConfigHelper\BeansConfig;
use PHPUnit\Framework\TestCase;

/**
 * bean辅助类测试
 * @author vijay
 */
class BeansConfigTest extends TestCase
{
    /**
     * 测试属性和别名生成
     * @return void
     */
    public function testPropsAndAlias()
    {
        self::assertEquals(
            [
                "mode" => "class",
                "class" => "cls",
                "props" => ["id" => "bbc"],
                "alias" => ["a"],
                "save" => false,
            ],
            BeansConfig::byClass(
                "cls",
                ["id" => "bbc"],
                ["a"],
                false
            )
        );
    }
}

# end of file
