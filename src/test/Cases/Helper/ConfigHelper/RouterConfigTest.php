<?php

namespace DreamCat\FrameCore\Cases\Helper\ConfigHelper;

use DreamCat\FrameCore\Helper\ConfigHelper\RouterConfig;
use PHPUnit\Framework\TestCase;

/**
 * 路由配置测试
 * @author vijay
 */
class RouterConfigTest extends TestCase
{
    /**
     * 测试配置方法
     * @return void
     */
    public function testMethod()
    {
        self::assertEquals(
            [
                "method" => "POST",
                "handle" => "ctl::act",
            ],
            RouterConfig::post("ctl", "act")
        );
        self::assertEquals(
            [
                "method" => "GET",
                "handle" => "ctl::act",
            ],
            RouterConfig::get("ctl", "act")
        );
    }
}

# end of file
