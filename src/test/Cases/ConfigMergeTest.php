<?php

namespace DreamCat\FrameCore\Cases;

use DreamCat\FrameCore\ConfigReaderImpl;
use PHPUnit\Framework\TestCase;

/**
 * 测试配置合并功能
 * @author vijay
 */
class ConfigMergeTest extends TestCase
{
    /**
     * 测试合并
     * @return void
     */
    public function testMerge()
    {
        $rootDir = __DIR__ . "/../resources";
        $configReader = new ConfigReaderImpl($rootDir);
        $expect = [
            "beans" => [
                "id1" => "cache",
                "id2" => "global",
                "id3" => "cache",
            ],
            "key" => [
                "id1" => "cache",
                "id2" => "global",
                "id3" => "cache",
            ],
            "k3" => [
                "id1" => "cache",
                "id3" => "cache",
            ],
        ];
        $config = $configReader->get("");
        self::assertEquals($expect, $config);
    }
}

# end of file
