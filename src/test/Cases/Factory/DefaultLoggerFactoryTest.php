<?php

namespace DreamCat\FrameCore\Cases\Factory;

use DreamCat\FrameCore\Factory\Impl\LoggerFactory\DefaultLoggerFactory;
use Monolog\Logger;
use PHPUnit\Framework\TestCase;

/**
 * 日志工厂类的测试用例
 * @author vijay
 */
class DefaultLoggerFactoryTest extends TestCase
{
    /**
     * 测试输出到终端的日志
     * @return void
     * @throws \Exception
     */
    public function testOutput()
    {
        # 创建日志类
        $factory = new DefaultLoggerFactory();
        /** @noinspection PhpUnhandledExceptionInspection */
        /** @var Logger $logger */
        $logger = $factory
            ->setLogConfig([
                "logs" => [],
                "stdout" => Logger::ERROR,
            ])
            ->create("", "");

        $logText = uniqid("test-");
        $output = "[" . date("Y-m-d H:i:s") . "] runlog.ERROR: {$logText} [] []\n";
        $this->expectOutputString($output);
        $logger->debug("此文本不会输出");
        $logger->error($logText);
    }
}

# end of file
