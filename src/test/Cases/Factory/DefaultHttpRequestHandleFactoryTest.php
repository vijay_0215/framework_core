<?php

namespace DreamCat\FrameCore\Cases\Factory;

use DreamCat\FrameCore\Factory\Impl\Container\DefaultContainerFactory;
use DreamCat\FrameCore\Factory\Impl\Controller\DefaultControllerFactory;
use DreamCat\FrameInterface\ConfigReader;
use FastRoute\Dispatcher;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\UriInterface;

/**
 * 默认的http请求工厂测试
 * @author vijay
 */
class DefaultHttpRequestHandleFactoryTest extends TestCase
{
    /**
     * 测试路由派发返回不正确的情况
     * @return void
     */
    public function testNotExpectResult()
    {
        $uriMock = $this->getMockForAbstractClass(UriInterface::class);
        $uriMock->expects(self::once())
            ->method("getPath")
            ->willReturn("/");
        $requestMock = $this->getMockForAbstractClass(ServerRequestInterface::class);
        $requestMock->expects(self::once())
            ->method("getMethod")
            ->willReturn("method");
        $requestMock->expects(self::once())
            ->method("getUri")
            ->willReturn($uriMock);
        $dispatchMock = $this->getMockForAbstractClass(Dispatcher::class);
        $dispatchMock->expects(self::once())
            ->method("dispatch")
            ->with(self::equalTo("method"), self::equalTo("/"))
            ->willReturn([-1]);

        # 设定预期的异常
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage("路由派发器出现预期之外的返回值-1");

        $configReader = $this->getMockForAbstractClass(ConfigReader::class);
        $configReader->method("get")->willReturnArgument(1);
        $container = (new DefaultContainerFactory())->create($configReader);
        /** @var DefaultControllerFactory $factory */
        $factory = $container->get(DefaultControllerFactory::class);
        $factory->setDispatcher($dispatchMock);
        self::assertEquals(spl_object_id($dispatchMock), spl_object_id($factory->getDispatcher()));
        $factory->httpHandle($container, $requestMock);
    }
}

# end of file
