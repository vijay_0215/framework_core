# framework_core

## 介绍
喵框架核心部分，目前只支持 FPM 入口，最终调用到控制器

## 安装教程
```bash
composer require dreamcat/cat_frame_core
```

## 使用说明
入口文件写以下代码即可
```php
<?php
\DreamCat\FrameCore\FpmEntrance::start();
```

如果入口文件不在根目录，则需要传入参数，表示根目录所在。后续所有说明都是基于根目录决定的文件路径。

### 控制器
控制器没有继承线的限制。如果更换了控制器工厂实现类，这一节内容失效

控制器处理消息的流程如下：
1. 遍历执行过滤器，如果得到返回值则向上返回，否则继续；
1. 调用路由器分析请求地址，如果找不到 handle 则返回，否则得到控制器类和方法名；
1. 遍历调用拦截器，如果得到返回值则向上返回，否则继续；
1. 调用控制器方法对应的协议对象 `convertInput` ，转换输入体；
1. 解析计划调用的方法注解，并根据注解解析入参，生成函数调用参数；
1. 调用控制器方法，得到返回值 `$data`；
1. 如果返回值未实现 `Psr\Http\Message\ResponseInterface` 接口，则调用协议对象的 `formatOutput` 函数得到标准输出；
1. 调用协议对象的 `convertOutput` 方法返回数据。

关于参数赋值与注解的写法：
1. 如果在函数注解类似于 `@param mixed $kk @PathVariable(label)` 则会将参数 `$kk` 赋值为 uri 中的占位符 `label` 的值
1. 如果参数注解类似于 `@param mixed $kk @RequestBody` 则
    - 如果参数没有类型标记或类型标记 (*要求在函数声明中加的类型定义*) 为 `string`，则将请求消息体赋值给 `$kk`
    - 如果是 `application/x-www-form-urlencoded` 或 `application/json` 会进行相应解码
1. 如果参数注解类似于 `@param mixed $kk @RequestBody(label)` 则将解码后数组 `label` 字段赋值给 `$kk`
1. 如果参数注解类似于 `@param mixed $kk @GetParam(label)` 则将 GET 参数中 `label` 字段赋值给 `$kk`，
如果没标明字段则将整个 GET 赋值给 `$kk`
1. 如果 `@param` 注解无定义，则依次尝试从 uri 的占位符或 GET 中以变量名为关键字获取数据
1. 如果上述方式都没有数据，则使用函数声明中的默认值
1. 如果没有定义默认值，根据类似传入类0值（即0、""、false、[]、null)

### 配置文件说明
配置文件的读取首先从 `configs/config.php` 中获取，然后再用 `cache/configs/config.php` 中的配置覆盖。
前者都是项目配置，后者则是为将来引入配置中心、扫描生成等方式预留。

下方关于配置路径均使用简写方式，各级以 `/` 分隔，
比如 `t1/t2` 配置为 `val` 表示配置的路径定义为 `["t1" => ["t2" => "val"]]`


### 配置详解
- `beans` `array` 默认情况下的容器装载配置，如果容器工厂使用自定义，容器配置则由实现类定义，键是对象ID，值可能的字段如下：
    - `alias` `string[]` 选填，别名列表，用对象ID或别名均可获取对象
    - `mode` `enum` 必填，装配模式，可能是以下几种可能性
        - `factory` 工厂模式
        - `class` 对象模式
    - `class` `string` 选填，默认值为ID。工厂模式下表示工厂类名，对象模式下表示对象类名本身
    - `props` `array` 选填，注入的属性列表。
    键表示属性名，值有几种可能性：
        - 如果是数组，存在两个字段
            - `val` `string`，含义由 `isVal` 决定
            - `isVal` `bool`，`false` 表示属性会从容器中获取ID为`val`相应值的对象，否则表示属性会直接赋值为`val`相应值
        - 如果值是字符串，表示属性的值

    如果值非数组 `$val`，相当于 `["class" => $val, "mode" => "class"]`

- `errorhandle/msg` `string` 选填，默认值是空字符串。异常时返回前台的错误信息。
- `errorhandle/code` `int` 选填，默认值是 `500`。异常时返回前台的http状态码。
- `http/notFoundCode` `int` 选填，默认值是 `404`，路由返回 NotFound 时的 http 状态码。
- `http/notFoundMsg` `string` 选填，默认值是 `not found`，路由返回 NotFound 时的提示信息。
- `http/notAllowedCode` `int` 选填，默认值是 `405`，路由返回 NotFound 时的 http 状态码。
- `http/notAllowedMsg` `string` 选填，默认值是 `not allowed`，路由返回 NotAllowed 时的提示信息。
- `protocol/act` `array` 选填，默认值为空。一级键是控制器类名，二级键是方法名，值是控制器方法的协议，必须实现接口 `DreamCat\FrameInterface\Controller\ProtocolInterface`。
- `protocol/ctl` `array` 选填，默认值是空。键是控制器类名，值是控制器未标注的方法使用的协议，必须实现接口 `DreamCat\FrameInterface\Controller\ProtocolInterface`。
- `protocol/default` `string` 选填，默认值是 `DreamCat\FrameCore\Protocol\JsonApiProtocol`，未特意标注的控制器方法协议，必须实现接口 `DreamCat\FrameInterface\Controller\ProtocolInterface`。
- `filters` `string[]` 选填，默认值是 `[]`，注册的过滤器列表，每个元素表示一个类名，必须实现接口 `DreamCat\FrameInterface\HttpHandle\FilterInterface`。
- `interceptors` `string[]` 选填，默认值是 `[]`，注册的拦截器列表，每个元素表示一个类名，必须实现接口 `DreamCat\FrameInterface\HttpHandle\InterceptorInterface`。

- `router` `array` 选填，默认值是 `[]`，路由配置信息。结构由路由派发器工厂实现类决定。
默认情况下结构如下
    - `prefix` `string` 选填，默认值是 `/`，uri前缀
    - `uris` `array` 必填，uri 配置。键是 uri 地址，值一定是数组或字符串，但是分三种情况
        - 值是字符串，则表示处理器标识，由 Http 请求处理器工厂的类决定，且 http 方法使用 *GET*
        默认的定义是以 `::` 分隔类名与方法名，方法名不存在则使用 `index` 方法，控制器类名必须继承于 `DreamCat\FrameCore\Controller\AbstractController`
        - 数组且存在 `method` 和 `handle` 字段，则可能的字段有
            - `method` `string` 必填，表示 http 方法
            - `handle` `string` 必填，处理器标识
        - 是数组且不存在 `method` 或 `handle` 字段，键表示前缀，值是组的 uri 配置

- `log` `array` 选填，默认值是 `[]`，日志信息，结构由日志工厂决定
    - `dir` `string` 选填，默认值是 `cache/logs`，表示日志根目录，如果目录地址以 `/` 开头表示绝对路径，否则为项目根目录为相对路径。
    - `stdout` `string` 选填，表示输出在标准输出的日志级别，不填则不输出
    - `name` `string` 选填，默认值是 `runlog`，表示日志渠道，在日志内容中体现。
    - `logs` `array` 选填，默认值是 `[['fileName' => "default.log", 'maxFile' => 10, 'level' => DEBUG]]`。
    日志 配置列表，每个元素都是一个日志处理器的Handle，目前使用的都是 `RotatingFileHandler`。配置元素结构：
        - `fileName` `string` 必填，表示日志文件名关键字
        - `maxFile` `string` 必填，表示日志文件最多保存多少个，0表示无限制
        - `level` `string` 必填，表示要处理的最低级别

### 默认 bean 配置
|  ID    | 类名                 | 模式   |
| :----: | :------------------: | :---: |
| `DreamCat\FrameInterface\HttpHandle\HttpRequestProcess` | `DreamCat\FrameCore\HttpHandle\DefaultHttpRequestProcess` | 对象 |
| `Zend\HttpHandlerRunner\Emitter\EmitterInterface` | `Zend\HttpHandlerRunner\Emitter\SapiEmitter` | 对象 |
| `Psr\Log\ErrorHandle` | `DreamCat\FrameCore\HttpHandle\DefaultErrorHandle` | 对象 |
| `Psr\Log\LoggerInterface` | `DreamCat\FrameCore\Factory\Impl\LoggerFactory\DefaultLoggerFactory` | 工厂 |
| `DreamCat\FrameInterface\Factory\ControllerFactory` | `DreamCat\FrameCore\Factory\Impl\Controller\DefaultControllerFactory` | 对象 |
| `FastRoute\Dispatcher` | `DreamCat\FrameCore\Factory\Impl\Dispatchers\DefaultDispatcherFactory` | 工厂 |
| `Psr\Http\Message\ServerRequestInterface` | `DreamCat\FrameCore\Factory\Impl\ServerRequest\DefaultServerRequestFactory` | 工厂 |
| `DreamCat\FrameInterface\Controller\ActionParamFactory` | `DreamCat\FrameCore\Factory\Impl\ActionParam\DefaultActionParamFactory` | 对象 |
| `DreamCat\Array2Class\Array2ClassInterface` | `DreamCat\Array2Class\Array2ClassConverter` | 对象 |

### 容器注解
- `@Config` 后面跟一个参数 `key` 为配置路径，从配置中读取配置，相当于 `ConfigReader->get('key')`。同时支持第二参数做为默认值
- `@RootPath` 获取项目根目录
   
## todo
1. 考虑接入配置中心的方式
1. 增加一个配置检查器，检查各项必要的配置是否完成
1. 接入命令行入口体系
1. 单元测试的测试用例解析器
1. 控制器参数解析完成后判断其合理性，尽量不要调用时才报错